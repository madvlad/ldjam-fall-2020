﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KeyBehavior : MonoBehaviour
{
    public AudioClip winSfx;
    public string nextLevel;

    public bool areYouReallyGonnaMakeMeChangeLevelsWhenYouCollectMeLikeYoureSomeKindOfEntitledPunk = true;

    void OnTriggerEnter2D(Collider2D other) {
        if (areYouReallyGonnaMakeMeChangeLevelsWhenYouCollectMeLikeYoureSomeKindOfEntitledPunk) {
            if (other.gameObject.CompareTag("Robot")) {
            Debug.Log("You win");
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            gameObject.GetComponent<AudioSource>().PlayOneShot(winSfx);
            Invoke("LoadNextLevel", 2f);
        }
        }
    }

    void LoadNextLevel() {
        SceneManager.LoadScene(nextLevel);
    }
}
