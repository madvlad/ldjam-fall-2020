﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 1f;
    public bool isGrounded;
    public LayerMask groundLayers;
    public AudioClip jumpSfx;

    void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapArea(
            new Vector2(transform.position.x - 0.04f, transform.position.y - 0.08f),
            new Vector2(transform.position.x + 0.04f, transform.position.y - 0.08f),
            groundLayers);

        bool isJump = false;
        if (Input.GetButton("Jump") && isGrounded) {
            isJump = true;
            Jump();
        }

        float dirX = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        float dirY = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        
        gameObject.GetComponent<MovementRecorder>().AddLog(new InputLog(dirX, dirY, isJump));

        transform.position = new Vector2(transform.position.x + dirX, transform.position.y);
    }

    void Jump()
    {
        gameObject.GetComponent<AudioSource>().PlayOneShot(jumpSfx);
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 5f), ForceMode2D.Impulse);
    }
}
