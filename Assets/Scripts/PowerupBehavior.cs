﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupBehavior : BasePowerupBehavior
{
    public AudioClip powerUpSfx;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Robot")) {
            other.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 10f), ForceMode2D.Impulse);
            gameObject.GetComponent<AudioSource>().PlayOneShot(powerUpSfx);
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
    }
}
