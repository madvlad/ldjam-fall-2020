﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BadZoneBehavior : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other) {
        Kill(other.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        Kill(other.gameObject);
    }

    private void Kill(GameObject other) {
        if (other.gameObject.CompareTag("Robot")) {
            var automaton = other.gameObject.GetComponent<AutomatonController>();
            if (gameObject.CompareTag("Enemy") && automaton.hasShield)
            {
                gameObject.GetComponent<EnemyBehavior>().Kill();
                automaton.UseShield();
            }
            else
            {
                automaton.Die();
            }
        }
    }
}
