﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AutomatonController : MonoBehaviour
{
    public float speed = 1f;
    public bool isGrounded;
    public bool hasShield;
    public LayerMask groundLayers;
    public GameObject playerObject;
    public GameObject home;
    public Animator animator;
    public SpriteRenderer spriteRenderer;
    public Rigidbody2D rigidbody2D;
    public AudioClip dieSfx;
    public AudioClip recordStartSfx;
    public AudioClip recordStopSfx;
    public AudioClip jumpSfx;
    public GameObject recordingIcon;

    private float lastXAxis = 0f;
    private float lastYAxis = 0f;

    private bool playbackOn = false;
    private int logIndex = 0;
    private List<InputLog> currentLoop;
    private MovementRecorder myMovementRecorder;
    private bool reverse = false;

    private void Start() {
        myMovementRecorder = playerObject.GetComponent<MovementRecorder>();
    }

    void Update() {
        if (Input.GetButtonDown("Fire1") && !playbackOn) {
            if (!myMovementRecorder.isRecording) {
                recordingIcon.SetActive(true);
                gameObject.GetComponent<AudioSource>().PlayOneShot(recordStartSfx);
                myMovementRecorder.isRecording = true;
            } else {
                recordingIcon.SetActive(false);
                gameObject.GetComponent<AudioSource>().PlayOneShot(recordStopSfx);
                myMovementRecorder.isRecording = false;
                playbackOn = true;
                currentLoop = playerObject.GetComponent<MovementRecorder>().inputQueue.Select(log => new InputLog(log.xAxis, log.yAxis, log.jump)).ToList();
            }
        }

        if (Input.GetButtonDown("Fire2")) {
            recordingIcon.SetActive(false);
            gameObject.GetComponent<AudioSource>().PlayOneShot(recordStopSfx);
            myMovementRecorder.isRecording = false;
            Die();
        }

        
        if (lastXAxis > 0) {
            spriteRenderer.flipX = false;
        } else if (lastXAxis < 0) {
            spriteRenderer.flipX = true;
        }
        animator.SetFloat("Horizontal", lastXAxis);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (playbackOn) {
            if (currentLoop.Count == 0) { return; }
            if (reverse && logIndex < 0) {
                logIndex = currentLoop.Count - 1;
            }
            if (!reverse && (logIndex >= currentLoop.Count || logIndex < 0)) {
                logIndex = 0;
            }

            if (logIndex >= 0 && logIndex <= currentLoop.Count - 1) {

                var nextPos = this.reverse ? new Vector2(transform.position.x + (-1*currentLoop[logIndex].xAxis) * speed, transform.position.y) : new Vector2(transform.position.x + currentLoop[logIndex].xAxis * speed, transform.position.y);
                transform.position = nextPos;
                lastXAxis = reverse ? -1 * currentLoop[logIndex].xAxis : currentLoop[logIndex].xAxis;
                if (currentLoop[logIndex].jump) Jump();
                logIndex = this.reverse ? logIndex - 1 : logIndex + 1;
            } else { 
                logIndex = 0;
            }
            
        }

        animator.SetFloat("Vertical", rigidbody2D.velocity.y);

        if (gameObject.transform.position.y < -100) {
            Die();
        }
    }

    void Jump()
    {
        animator.SetTrigger("LoadJump");
        gameObject.GetComponent<AudioSource>().PlayOneShot(jumpSfx);
        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 7f), ForceMode2D.Impulse);
    }

    public void Die() {
        ResetPowerups();
        ResetEnemies();
        playbackOn = false;
        playerObject.GetComponent<MovementRecorder>().Clear();  
        transform.position = home.transform.position;
        lastXAxis = 0f;
        gameObject.GetComponent<AudioSource>().PlayOneShot(dieSfx);
        this.reverse = false;
    }

    public void Reverse() {
        this.reverse = !reverse;
    }

    public void UseShield()
    {
        hasShield = false;
        gameObject.GetComponent<AudioSource>().PlayOneShot(dieSfx);
    }

    
    private void ResetPowerups()
    {
        var powerups = GameObject.FindGameObjectsWithTag("Powerup");
        foreach (var powerup in powerups)
        {
            powerup.GetComponent<BasePowerupBehavior>().Activate();
        }
    }

    private void ResetEnemies()
    {
        var enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var enemy in enemies)
        {
            enemy.GetComponent<EnemyBehavior>().Reload();
        }
    }
}
