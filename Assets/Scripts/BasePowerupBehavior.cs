using UnityEngine;

public class BasePowerupBehavior : MonoBehaviour {
    public void Activate() {
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
    }
}