﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class EnemyBehavior : MonoBehaviour
{
    public float timer = 4f;
    public float walkSpeed = 1f;
    public float rotationSpeed = 1f;
    public float jumpPower = 1f;
    public bool jumps = false;
    public bool rotates = false;
    private float clock = 0.0f;
    private bool goingRight = false;
    public AudioClip dieSfx;
    private Vector3 initialPosition;
    private Quaternion initialRotation;
    
    // Update is called once per frame


    private void Start()
    {
        initialPosition = transform.position;
        initialRotation = transform.rotation;
    }
    void Update()
    {
        
        clock += Time.deltaTime;
        if (clock >= timer) {
            clock = 0.0f;
            if (jumps) {
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 5f * jumpPower), ForceMode2D.Impulse);
            }
            goingRight = !goingRight;
        }

        float dirX = walkSpeed * Time.deltaTime;

        if (!goingRight) dirX *= -1;

        if (rotates) {
            // transform.rotation = new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z  + rotationSpeed, transform.rotation.w);
            transform.Rotate(0f, 0f, rotationSpeed);
        } else {
            transform.position = new Vector2(transform.position.x + dirX, transform.position.y);
        }
    }

    public void Reload()
    {
        clock = 0;
        goingRight = false;
        transform.position = initialPosition;
        transform.rotation = initialRotation;
        gameObject.SetActive(true);
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
    }

    public void Kill()
    {
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
    }
}
