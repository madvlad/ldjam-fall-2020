﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialNavigator : MonoBehaviour
{
    public List<GameObject> TutorialSlides;
    public int CurrentIndex = 0;
    public AudioClip SoundEffect;

    void Update() {
        if (Input.GetButtonDown("Submit")) {
            if (CurrentIndex < TutorialSlides.Count) {
                gameObject.GetComponent<AudioSource>().PlayOneShot(SoundEffect);
                TutorialSlides[CurrentIndex].SetActive(false);
                CurrentIndex++;
                if (CurrentIndex < TutorialSlides.Count) {
                    TutorialSlides[CurrentIndex].SetActive(true);
                    
                }
            }
        }
    }
}
