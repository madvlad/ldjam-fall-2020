public class InputLog
{
    public InputLog(float x, float y, bool jump) {
        xAxis = x;
        yAxis = y;
        this.jump = jump;
    }
    
    public string logMe() {
        return xAxis + " " + yAxis + " " + jump;
    }
    
    public float xAxis { get; set; }
    public float yAxis { get; set; }
    public bool jump { get; set; }
}