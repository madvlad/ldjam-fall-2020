﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterBehavior : MonoBehaviour
{
    public GameObject teleOut;

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.CompareTag("Player")) {
            Vector2 newPosition = new Vector2(teleOut.transform.position.x, other.gameObject.transform.position.y);
            other.gameObject.transform.position = newPosition;
        }
    }
}
