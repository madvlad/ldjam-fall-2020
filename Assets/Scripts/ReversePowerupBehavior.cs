﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReversePowerupBehavior : BasePowerupBehavior
{
    public AudioClip powerUpSfx;

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Robot")) {
            other.gameObject.GetComponent<AutomatonController>().Reverse();
            gameObject.GetComponent<AudioSource>().PlayOneShot(powerUpSfx);
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
    }
}
