﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementRecorder : MonoBehaviour
{
    public List<InputLog> inputQueue;
    public float QueueDeadline = 1f;
    public bool isRecording = false;

    private float timer = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        inputQueue = new List<InputLog>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > QueueDeadline) {
            timer = 0.0f;
            if (inputQueue.Count > 0)
                inputQueue.RemoveAt(0);
        }
    }

    public void AddLog(InputLog log) {
        if (isRecording) {
            inputQueue.Add(log);
        }
    }

    public void Clear() {
        inputQueue.Clear();
    }
}
